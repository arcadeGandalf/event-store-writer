﻿using System;
using System.Net;
using System.Text;
using EventStore.ClientAPI;

namespace event_store_writer
{
    class Program
    {
        static void Main(string[] args)
        {
            const string STREAM = "Vehicle";
            const int DEFAULTPORT = 1113;
            //uncomment to enable verbose logging in client.
            var settings = ConnectionSettings.Create();//.EnableVerboseLogging().UseConsoleLogger();

            var write = true;
            var count = 1;

            while (write)
            {
                using var conn = EventStoreConnection.Create(settings, new IPEndPoint(IPAddress.Loopback, DEFAULTPORT));
                conn.ConnectAsync().Wait();
                conn.AppendToStreamAsync($"{STREAM}-{count}",
                    ExpectedVersion.Any,
                    GetEventDataFor(count)).Wait();
                Console.WriteLine("event " + count + " written.");
                count++;
                var key = Console.ReadKey();

                if (key.KeyChar == 'q')
                {
                    write = false;
                }
            }
        }

        private static EventData GetEventDataFor(int i)
        {
            return new EventData(
                Guid.NewGuid(),
                "Ordered",
                true,
                Encoding.ASCII.GetBytes("{\"orderId\" : " + i + "}"),
                Encoding.ASCII.GetBytes("{\"orderId\" : " + i + "}")
                );
        }
    }
}
